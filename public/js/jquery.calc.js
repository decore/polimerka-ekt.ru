// localStorage.clear(); // Для отладки
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
// Распарсиваем json в localstorage (значения являются строчками)
$.getJSON('js/price.json', {}, function(json){
	localStorage.setItem('price', JSON.stringify(json) );
});
$.getJSON('js/color.json', {}, function(json){
	localStorage.setItem('color', JSON.stringify(json) );
});
$.getJSON('js/position.json', {}, function(json){
	localStorage.setItem('blankPosition', JSON.stringify(json) );
});
// Распарсиваем строчки в localstorage и передаем значения переменным
var color = $.parseJSON( localStorage.getItem('color') );
var price = $.parseJSON( localStorage.getItem('price') );
var blankPosition = $.parseJSON( localStorage.getItem('blankPosition') );
// Переменная, где хранятся цвета как новые объекты к позиции
localStorage.setItem('activeColors', '[]');
var activeColors = $.parseJSON( localStorage.getItem('activeColors') );
// Создание в localstorage поля для position (массив строк таблицы)
if ( localStorage.getItem('position') == null ) {
	localStorage.setItem('position', '[]');
}
var position = $.parseJSON( localStorage.getItem('position') );
// Ф-ия добавления пустой позиции
addPosition = function() {
	position = $.parseJSON( localStorage.getItem('position') );
	position[position.length] = blankPosition;
	localStorage.setItem( 'position', JSON.stringify(position) );
}
// Добавим первую строчку
addPosition();
// Ф-ия удаления позиции
removePosition = function(positionNumber) {
	position = $.parseJSON( localStorage.getItem('position') );
	position.splice(positionNumber, 1); // Удаляем 1 элемент по номеру positionNumber
	localStorage.setItem( 'position', JSON.stringify(position) );
}
refreshNumber = function(){
	var i = 0;
	$('#calculator tbody tr').each(function(){
		$(this).find('.number').text(i);
		i++;
	});
}
ifChangeTable = function(){
	var totalSum = 0; 
	for (var i=2;i<position.length+2;i++){
		var td = $('#calculator tbody tr:nth-child('+i+')');
		// Заносим .input_color в position
		position[i-2][0].id = td.find('.input_color').val();
		// По id заполнить hex и group
		for(var b=0; b<color.length; b++) {
			if( color[b].id == position[i-2][0].id ){
				position[i-2][0].hex = color[b].hex;
				position[i-2][0].group = color[b].group;
				break
			};
		}
		if ( td.find('.input_color').val() ) {
			td.find('.color_preview div').removeClass('active').css({'background-image': 'none', 'background-color': position[i-2][0].hex});
		}
		else {
			td.find('.color_preview div').css({'background-color': 'none'}).addClass('active');
			position[i-2][0].id = '';
			position[i-2][0].hex = '';
			position[i-2][0].group = '';
		}
 
		td.find('.input_color').val(position[i-2][0].id);
		// Заносим select
		position[i-2][0].select = td.find('select').val();
		// Заносим input
		position[i-2][1][0][0] = td.find('.type_1 .control-group:nth-child(1) input').val();
		position[i-2][1][0][1] = td.find('.type_1 .control-group:nth-child(2) input').val();
		position[i-2][1][0][2] = td.find('.type_1 .control-group:nth-child(3) input').val();
		position[i-2][1][0][3] = td.find('.type_1 .control-group:nth-child(4) input').val();
		
		position[i-2][1][1][0] = td.find('.type_2 .control-group:nth-child(1) input').val();
		position[i-2][1][1][1] = td.find('.type_2 .control-group:nth-child(2) input').val();
		position[i-2][1][1][2] = td.find('.type_2 .control-group:nth-child(3) input').val();
		position[i-2][1][1][3] = td.find('.type_2 .control-group:nth-child(4) input').val();

		position[i-2][1][2][0] = td.find('.type_3 .control-group:nth-child(1) input').val();
		position[i-2][1][2][1] = td.find('.type_3 .control-group:nth-child(2) input').val();

		position[i-2][1][3][0] = td.find('.type_4 .control-group:nth-child(1) input').val();
		position[i-2][1][3][1] = td.find('.type_4 .control-group:nth-child(2) input').val();
		position[i-2][1][3][2] = td.find('.type_4 .control-group:nth-child(3) input').val();
		position[i-2][1][3][3] = td.find('.type_4 .control-group:nth-child(4) input').val();
		// ...
		// Заносим checkbox
		position[i-2][2][0] = td.find('.checkbox:nth-child(1) input').prop('checked');
		position[i-2][2][1] = td.find('.checkbox:nth-child(2) input').prop('checked');
		position[i-2][2][2] = td.find('.checkbox:nth-child(3) input').prop('checked');
		position[i-2][2][3] = td.find('.checkbox:nth-child(4) input').prop('checked');
		position[i-2][2][4] = td.find('.checkbox:nth-child(5) input').prop('checked');
		position[i-2][2][5] = td.find('.checkbox:nth-child(6) input').prop('checked');
		// ...
		// Меняем картинку для select
		var blueprintVal = td.find('select').val();
		if( blueprintVal == 0 ) {
			td.find('.blueprint').hide();
			td.find('.type_1, .type_2, type_3, type_4').addClass('hide');
		} else {
			td.find('.blueprint').html('<img src="img/blueprint/' + blueprintVal + '.png">').show();
			td.find( '.type_' + blueprintVal ).removeClass('hide').siblings().addClass('hide');
		}
		// СЧИТАЕМ строчку
		var length = 0;
		var height = 0;
		var count = 0;
		var volume = 0;
		var length_section = 0;
		var width_section = 0;
		var max_length = 0;
		var perimeter = 0;
		var sum = 0;
		var markup1 = 1;
		var markup2 = 1;
		var markup3 = 1;
		var markup4 = 1;
		var markup5 = 1;
		var markup6 = 1;
		// Получаем наценки для строчки
		if ( position[i-2][2][0] ){
			markup1 = td.find('.checkbox:nth-child(1) input').val();
		}
		if ( position[i-2][2][1] ){
			markup2 = td.find('.checkbox:nth-child(2) input').val();
		 	// Если масса больше 150кг, то не учитывать вес 100-150кг
			markup1 = 1;
		}
		if ( position[i-2][2][2] ){
			markup3 = td.find('.checkbox:nth-child(3) input').val();
		}
		if ( position[i-2][2][3] ){
			markup4 = td.find('.checkbox:nth-child(4) input').val();
		}
		if ( position[i-2][2][4] ){
			markup5 = td.find('.checkbox:nth-child(5) input').val();
		}
		if ( position[i-2][2][5] ){
			markup6 = td.find('.checkbox:nth-child(6) input').val();
		}
		// Считаем в зависимости от типа изделия
		// Пустой блок
		if (position[i-2][0].select == 0) {
			td.find('.input_price').text('- руб.');
		}
		// Плоские изделия
		if (position[i-2][0].select == 1) {
			length = parseInt(position[i-2][1][0][0]);
			width = parseInt(position[i-2][1][0][1]);
			height = parseInt(position[i-2][1][0][2]);
			count = parseInt(position[i-2][1][0][3]);
			if( !count ){ count=0 }
			volume = count * 2 * (length + width + height);
			// ...
			for(var b=0; b<price[0].length; b++){
				if( volume >= price[0][b][0] ){
					// Умножаем кол-во метров на стоимость метра
					// Умножаем на наценки
					if(position[i-2][0].group) {
						sum = volume * price[0][b][position[i-2][0].group] * markup1 * markup2 * markup3 * markup4 * markup5 * markup6;
						break
					}
					else {
						sum = 0;
					}
				}
			}
		}
		// Длинномерные изделия
		if (position[i-2][0].select == 2) {
			length_section = parseInt(position[i-2][1][1][0]);
			width_section = parseInt(position[i-2][1][1][1]);
			length = parseInt(position[i-2][1][1][2]);
			count = parseInt(position[i-2][1][1][3]);
			if( !count ){ count=0 }
			perimeter = 2 * (length_section + width_section);
			volume = perimeter * length * count;
			for(var b=0; b<price[1].length; b++){
				if( perimeter >= price[1][b][0] ){
					// Умножаем кол-во метров на стоимость метра
					// Умножаем на наценки
					sum = volume * price[1][b][1] * markup1 * markup2 * markup3 * markup4 * markup5 * markup6;
					break;
				}
			}
		}
		// Малоразмерные изделия
		if (position[i-2][0].select == 3) {
			count = 0; /////
			max_length = parseInt(position[i-2][1][2][0]);
			count = parseInt(position[i-2][1][2][1]);
			if( !count ){ count=0 }
			for(var b=0; b<price[2].length; b++){
				if( max_length <= price[2][b][0] ){
					// Умножаем кол-во изделий на наценки
					sum = count * price[2][b][1] * markup1 * markup2 * markup3 * markup4 * markup5 * markup6;
					break;
				}
			}
		}
		// Объемные изделия
		if (position[i-2][0].select == 4) {
			length = parseInt(position[i-2][1][3][0]);
			width = parseInt(position[i-2][1][3][1]);
			height = parseInt(position[i-2][1][3][2]);
			count = parseInt(position[i-2][1][3][3]);
			if( !count ){ count=0 }
			volume = count * 2 * (length + width + height);
			// ...
			for(var b=0; b<price[0].length; b++){
				if( volume >= price[0][b][0] ){
					// Умножаем кол-во метров на стоимость метра
					// Умножаем на наценки
					if(position[i-2][0].group) {
						sum = volume * price[3][b][position[i-2][0].group] * markup1 * markup2 * markup3 * markup4 * markup5 * markup6;
						break
					}
					else {
						sum = 0;
					}
				}
			}
		}
		// parseInt для sum
		sum = parseInt(sum);
		td.find('.input_price').text(sum + ' руб.');
		// Считаем итоговую стоимость
		var totalSum = totalSum + sum;
		$('#input_price_count').text('Общая стоимость: ' + totalSum + ' руб.');
	}
	// Заносим все localstorage
	localStorage.setItem( 'position', JSON.stringify(position) );
}
fillTable = function(){
	position = $.parseJSON( localStorage.getItem('position') );
	if (position.length) {
		for (var i=2;i<position.length+2;i++){
			$('#calculator tbody').append( '<tr>' + $('#blank_position').html() + '</tr>' );
			// Заносим id в .input_color
			$('#calculator tbody tr:nth-child('+i+')').find('.input_color').val(position[i-2][0].id);
			// Заносим select
			$('#calculator tbody tr:nth-child('+i+')').find('select').val(position[i-2][0].select);
			// Заносим input
			$('#calculator tbody tr:nth-child('+i+')').find('.type_1 .control-group:nth-child(1) input').val(position[i-2][1][0][0]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_1 .control-group:nth-child(2) input').val(position[i-2][1][0][1]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_1 .control-group:nth-child(3) input').val(position[i-2][1][0][2]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_1 .control-group:nth-child(4) input').val(position[i-2][1][0][3]);

			$('#calculator tbody tr:nth-child('+i+')').find('.type_2 .control-group:nth-child(1) input').val(position[i-2][1][1][0]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_2 .control-group:nth-child(2) input').val(position[i-2][1][1][1]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_2 .control-group:nth-child(3) input').val(position[i-2][1][1][2]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_2 .control-group:nth-child(4) input').val(position[i-2][1][1][3]);

			$('#calculator tbody tr:nth-child('+i+')').find('.type_3 .control-group:nth-child(1) input').val(position[i-2][1][2][0]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_3 .control-group:nth-child(2) input').val(position[i-2][1][2][1]);

			$('#calculator tbody tr:nth-child('+i+')').find('.type_4 .control-group:nth-child(1) input').val(position[i-2][1][3][0]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_4 .control-group:nth-child(2) input').val(position[i-2][1][3][1]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_4 .control-group:nth-child(3) input').val(position[i-2][1][3][2]);
			$('#calculator tbody tr:nth-child('+i+')').find('.type_4 .control-group:nth-child(4) input').val(position[i-2][1][3][3]);
			// ...
			// Заносим checkbox
			$('#calculator tbody tr:nth-child('+i+')').find('.checkbox:nth-child(1) input').prop( 'checked', position[i-2][2][0] );
			$('#calculator tbody tr:nth-child('+i+')').find('.checkbox:nth-child(2) input').prop( 'checked', position[i-2][2][1] );
			$('#calculator tbody tr:nth-child('+i+')').find('.checkbox:nth-child(3) input').prop( 'checked', position[i-2][2][2] );
			$('#calculator tbody tr:nth-child('+i+')').find('.checkbox:nth-child(4) input').prop( 'checked', position[i-2][2][3] );
			$('#calculator tbody tr:nth-child('+i+')').find('.checkbox:nth-child(5) input').prop( 'checked', position[i-2][2][4] );
			$('#calculator tbody tr:nth-child('+i+')').find('.checkbox:nth-child(6) input').prop( 'checked', position[i-2][2][5] );
			// ...
		}
		ifChangeTable();
	}
	else {
		$('#calculator tbody').append( '<tr>' + $('#blank_position').html() + '</tr>' );
	}
}
// PRICE
$('#calculator').is(function(){
	// Заполняем таблицу
	fillTable();
	refreshNumber();
	// Кнопка добавления позиции
	$('#add_position').live('click', function(){
		$('#calculator tbody').append( '<tr>' + $('#blank_position').html() + '</tr>' );
		addPosition();
		refreshNumber();
	});
	// Кнопка удаления позиции
	$('.delete_position').live('click', function(e){
		e.preventDefault();
		$(this).parents('tr').remove();
		removePosition( parseInt($(this).parents('tr').find('.number').text()) - 1 );
		refreshNumber();
		ifChangeTable();
	});
	// При изменении хотя бы одного поля
	$('#calculator input, #calculator select').live('change', function(){
		ifChangeTable();
	});
});
// COLOR
$('#color_preview_big').is(function(){
	// Наполняем страницы цветами
	for(var i=0; i<color.length; i++) {
		$('#group_' + color[i].group).append('<div class="span1"><a class="color thumbnail"><div style="background-color: ' + color[i].hex + '"><span>' + color[i].id + '</span></div></a></div>');
	}
	// Привязка предпросмотра цвета к верху экрана при скроле
	var scrollColor = $('#color_preview_big div').position().top;
	$(window).scroll(function() {
	    if ( $(window).scrollTop() > scrollColor ) {
	        $('#color_preview_big div').addClass('active');
	    } else {
	        $('#color_preview_big div').removeClass('active');
	    }
	});
	$('#input_add_color').click(function(e){
		e.preventDefault();
		// Добавляем цвета в position
		for (i=0;i<activeColors.length;i++){
			position.push(activeColors[i]);
		}
		localStorage.setItem( 'position', JSON.stringify(position) );
		window.location.href = 'price.html#calculator';
	});
	// Предпросмотр цвета, добавление цвета в localstorage
	$('.color').live('click', function(){
		var click_to = $(this);
		var click_to_id = click_to.find('span').text();
		var click_to_hex = rgb2hex( click_to.find('div').css('background-color') );
		var click_to_group;
		// Отображение предпросмотра цвета
		$('#color_preview_big div').show().css({
			'background-image': 'none',
			'background-color': click_to_hex
		});
		// Добавление цвета в список
		click_to.toggleClass('active');
		activeColors = $.parseJSON( localStorage.getItem('activeColors') );
		if ( click_to.attr('class') == 'color thumbnail active' ){
			for(var i=0; i<color.length; i++) {
				if( color[i].hex == click_to_hex ){
					click_to_group = color[i].group;
					break
				};
			}
			// Заносим цвет в activeColors
			activeColors[activeColors.length] = blankPosition;
			activeColors[activeColors.length - 1][0].id = click_to_id;
			activeColors[activeColors.length - 1][0].hex = click_to_hex;
			activeColors[activeColors.length - 1][0].group = click_to_group;
		}
		// Удаление цвета из списка
		else {
			for(var i=0; i<activeColors.length; i++) {
				if( activeColors[i][0].hex == click_to_hex ){
					activeColors.splice(i, 1); // Удаляем 1 элемент по номеру positionNumber
					break
				};
			}
		}
		localStorage.setItem( 'activeColors', JSON.stringify(activeColors) );	
	});
});
Base project on serve
=============

Learn more about serve:

<https://github.com/jlong/serve/>

How install and run project?
-------------------------------

1. Install ruby
2. Install rubygems
3. Install bundle:

    gem install bundle

4. Install gems:

    bundle install

5. Run serve:

    serve

6. http://localhost:3000